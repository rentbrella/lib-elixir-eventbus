# Pub

Performs interactions with Rentbrella EventBus

## Initial creation

This library is created with the following command:

```bash
mix new --module Pub --sup --app rb_eventbus lib-eventbus
```

## Installation

Add in your `mix.exs`:

```elixir
  defp deps do
    [
      # another packages here ...
      {:rb_eventbus, "0.1.0", organization: "rentbrella"}
    ]
  end
```

## Configuration

### Environment variables

This library depends of the following environment variables to work correctly:

* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`
* `AWS_SQS_QUEUE`
* `AWS_SNS_TOPIC_ARN`

### Callback functions

Configure callback functions according bellow:

```elixir
config :rb_eventbus,
  callbacks: [
    {"user_blocked",      &MyCoolModule.my_func1/1},
    {"movement_created",  &MyCoolModule.my_func2/1},
    {"movement_returned", &MyCoolModule.my_func3/1}
  ]
```

These functions will be called whenever a new message is arrived by SQS. The argument of this function will be a struct `%SQSMessage{}` representing the received message.

Note that is possible set up different functions for different events.

## Usage

### Publishing a message

```elixir
alias Pub
alias Pub.Event

{:ok, event} = Event.new("user_blocked", %{"user_id" => 1})

EventBus.publish(event)
```

### Receiving a message

```elixir
alias Pub

EventBus.get_message()
```