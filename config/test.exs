use Mix.Config

config :rb_eventbus,
  callbacks: [
    {"dummy_event", fn _ -> nil end}
  ]
