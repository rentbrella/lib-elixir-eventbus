defmodule Pub.MixProject do
  use Mix.Project

  def project do
    [
      app: :rb_eventbus,
      version: "2.1.4",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: docs(),
      name: "EventBus",
      source_url: "https://bitbucket.org/rentbrella/lib-elixir-eventbus",
      description: description(),
      package: package(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Pub.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.1"},
      {:sweet_xml, "~> 0.7.1"},
      {:hackney, "~> 1.15"},
      {:ex_aws, "~> 2.1"},
      {:ex_aws_sqs, "~> 3.0"},
      {:ex_aws_sns, "~> 2.0"},
      {:gen_stage, "~> 1.0"},
      {:ex_doc, "~> 0.25.5", only: :dev, runtime: false}
    ]
  end

  defp description() do
    "Rentbrella's EventBus package."
  end

  defp package() do
    [
      organization: "Rentbrella",
      name: "rb_eventbus",
      licenses: [""],
      links: %{
        "BitBucket" => "https://bitbucket.org/rentbrella/lib-elixir-eventbus"
      }
    ]
  end

  defp docs() do
    [
      main: "Pub",
      groups_for_modules: [
        "Main Module": [
          Pub
        ],
        "Data Structs": [
          Pub.Event,
          Pub.SQSMessage
        ],
        "Queue Interactions": [
          Pub.Queue.Receiver,
          Pub.Queue.Publisher,
          Pub.Queue.Acknowledger
        ],
        "GenStage Modules": [
          Pub.Workers.Consumer,
          Pub.Workers.Producer,
          Pub.Workers.ProducerConsumer
        ]
      ]
    ]
  end
end
