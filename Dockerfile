# Neither alpine nor slim versions play well with AppSignal, don't bother trying
# Based on example from: https://hexdocs.pm/phoenix/releases.html#containers

FROM elixir:latest

ENV MIX_ENV=test

# prepare build dir
RUN mkdir -p /srv/lib

WORKDIR /srv/lib

COPY . .

# install hex + rebar
RUN mix local.hex --force
RUN mix local.rebar --force

# install mix dependencies
RUN mix deps.get

# precompiling
RUN mix deps.compile
RUN mix compile

CMD mix test
