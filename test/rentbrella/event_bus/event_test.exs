
defmodule Pub.EventTest do
  use ExUnit.Case

  alias Pub.Event

  doctest Pub.Event

  describe "Event.new/1" do
    test "converts payload fields in atoms" do
      result =
        "user_blocked"
        |> Event.new(%{"user_id" => Enum.random(1..10)})

      {:ok, %Event{payload: %{user_id: _}}} = result
    end
  end
end
