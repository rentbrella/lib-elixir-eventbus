defmodule Pub.Event do
  @moduledoc "Represents a Event"

  @derive Jason.Encoder
  defstruct [:payload, :event]

  @type t :: %__MODULE__{
          payload: map(),
          event: binary()
        }

  alias __MODULE__

  @doc """
  Creates new Event's struct

  ### Examples
      iex> Pub.Event.new("user_blocked", %{"user_id" => 1})
      {:ok,
        %Pub.Event{
        event: "user_blocked",
        payload: %{user_id: 1}
      }}
  """
  @spec new(binary(), map()) :: {:ok, t()} | {:error, atom, list}
  def new(event_name, %{} = payload) when is_binary(event_name) do
    %Event{}
    |> Map.put(:event, event_name)
    |> Map.put(:payload, payload)
    |> transform()
  end

  defp transform(%{payload: payload} = event) do
    payload = payload
              |> Enum.reduce(%{}, fn {key, value}, acc ->
                key =
                  if is_atom(key) do
                    key
                  else
                    String.to_atom(key)
                  end

                Map.put(acc, key, value)
              end)
    {:ok, Map.put(event, :payload, payload)}
  end
end
