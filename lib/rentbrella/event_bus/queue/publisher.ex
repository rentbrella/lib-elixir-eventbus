defmodule Pub.Queue.Publisher do
  alias ExAws.SNS

  alias Pub.Event

  require Logger

  @doc "Sends a `event` to a SNS `topic`"
  @spec run(Event.t() | {:ok, Event.t()}, binary) :: {:ok, map} | {:error, any}
  def run(event),
    do: run(event, System.get_env("AWS_SNS_TOPIC_ARN"))
  def run({:ok, event = %Event{}}, topic_arn) do
    run(event, topic_arn)
  end
  def run(%Event{payload: payload, event: event_name}, topic_arn) do
    case Jason.encode(payload) do
      {:ok, json} -> publish(event_name, json, topic_arn)
      another -> another
    end
  end

  defp publish(event_name, encoded_payload, topic_arn) do
    opts = [
      target_arn: topic_arn,
      message_attributes: [
        %{name: "event",
          data_type: :string,
          value: {:string, event_name}}
      ]
    ]

    case System.get_env("AWS_ACCESS_KEY_ID") do
      "silently_ignore" ->
        Logger.warn("Pub.Queue.Publisher.publish::silently ignoring...")
        {:ok, %{}}
      _ ->
        encoded_payload
        |> SNS.publish(opts)
        |> ExAws.request()
    end
  end
end